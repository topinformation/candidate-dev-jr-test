# Objetivo do Teste

* Criar uma aplicação que segregada em Front End e Back End
* Persitir os dados em banco de dados
* Cobertura de pelo menos 30% teste de unidade
* Deploy em servidores como : AWS, Heroku, Azure, etc
* Código legivel, seguir os padrões de clean code
* Aplicação Responsiva
* Design Elegante
* Aplicação de Padrões de Projeto

# Pontos Extra

* Cobertura de Testes
* Use Lightning Design - http://lightningdesignsystem.com/
