# Objetivo do Teste

* :ballot_box_with_check: Criar uma aplicação que segregada em Front End e Back End
* :ballot_box_with_check: Persitir os dados em banco de dados
* :ballot_box_with_check: Pelo menos um teste de unidade
* :ballot_box_with_check: Deploy em servidores como : AWS, Heroku, Azure, etc
* :ballot_box_with_check: Código legivel, seguir os padrões de clean code

# Pontos Extra

* :heavy_plus_sign: Cobertura de Testes
* :heavy_plus_sign: Aplicação Responsiva
* :heavy_plus_sign: Use Lightning Design - http://lightningdesignsystem.com/
* :heavy_plus_sign: Design Elegante
* :heavy_plus_sign: Aplicação de Padrões de Projeto