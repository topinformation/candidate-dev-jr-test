# Time Tracking APP

## Objetivo do Teste

* :ballot_box_with_check: Criar uma aplicação segregada em Front End e Back End
* :ballot_box_with_check: Persitir os dados em banco de dados
* :ballot_box_with_check: Pelo menos um teste de unidade
* :ballot_box_with_check: Deploy em servidores como : AWS, Heroku, Azure, etc
* :ballot_box_with_check: Código legivel, seguir os padrões de clean code

## Pontos Extra

* :heavy_plus_sign: Cobertura de Testes
* :heavy_plus_sign: Aplicação Responsiva
* :heavy_plus_sign: Use Lightning Design - http://lightningdesignsystem.com/
* :heavy_plus_sign: Design Elegante
* :heavy_plus_sign: Aplicação de Padrões de Projeto


# Repositório
* Você precisa criar um repositório privado e adicionar os e-mails abaixo para revisão do seu código:

    * rafael.colatusso@topi.io
    * eduardo.carvalho@topi.io
    * oseas.tormen@topi.io
    * watson.marconato@topi.io

---
## Contexto de Negócio

Um de nossos Clientes nos solicitou uma aplicação/aplicativo, que controlasse o apontamento de Horas, de seu grupo de colaboradores, ele nos pediu que deveriamos considerar as regras abaixo

* Aplicação poderá ser utilizada somente por usuários com o perfil 'EMPLOEE'
* Deverá ter 3 categorias de apontamento. sendo eles
    * Padrão
        * Permitir apontar somente 8 horas por dia
    * Hora Extra
        * Somente se o usuário possuir permissão de apontamento de hora extra
        * Não pode ultrapassar de 80h no mês
    * Banco de Horas
        * Somente se o usuário possuir permissão de apontamento de Banco de horas
        * Não pode ultrapassar de 40h no mês

* Não permitir a criação de dois apontamentos no mesmo intervalo do dia, ou seja não permitir mais de um apontamento no mesmo horario do dia.
* Utilizar a base de Projetos existente para determinar as tarefas dos Colaboradores
* Exibir somente tarefas liberadas e atribuídas no usuário, na base de projetos existentes.


## User Story

**Sendo** um usuário com perfil de “EMPLOYEE” 

**Posso** realizar o apontamento de horas para minhas atividades liberadas

**Para** que assim o financeiro possa fazer a apuração mensal do total de horas realiza e realizar o pagamento


### CA01 - Listar Apontamentos por um determinado período

**Dado** que sou um usuário com o perfil de “EMPLOYEE”
 
&nbsp;&nbsp;&nbsp;&nbsp;**E** estou na tela de “Log Report” 

**Quando** seleciono o Intervalo de Datas válido  

**Então** deverá ser apresentado os meus apontamentos, realizados conforme o intervalo de data selecionado, agrupados por Data - FIG - USER LOG REPORT


### CA02 - Adicionar um novo apontamento

**Dado** que sou um usuário com o perfil de “EMPLOYEE”

&nbsp;&nbsp;&nbsp;&nbsp;**E** estou na tela de “Log Report”

**Quando** clico no botão “Log Time”

**Então** deverá ser apresentada a tela para adicionar um novo apontamento
( fig  - modal add/edit - log time )


### CA03 - Novo apontamento “Padrão” adicionado com sucesso

**Dado** estou visualizando a tela de Novo apontamento ( fig  - modal add/edit - log time )

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei uma Tarefa  atribuída a mim

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei o Tipo como Padrão

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei uma Data valida

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei um intervalo de início e Fim válidos.


**Quando** clico no botão “Log Time”

**Então** será adicionado um novo apontamento para mim
    
&nbsp;&nbsp;&nbsp;&nbsp;**E**  a ele será referenciado a tarefa e a quantidade de horas executadas na mesma.

&nbsp;&nbsp;&nbsp;&nbsp;**E** será exibida uma mensagem de Sucesso “Log Time adicionado com sucesso”.

&nbsp;&nbsp;&nbsp;&nbsp;**E** o modal será fechado de forma automática.

&nbsp;&nbsp;&nbsp;&nbsp;**E** o novo apontamento deverá ser exibido na tela de “Log Time Report ”


### CA04 - Erro de campos obrigatórios não preenchidos

**Dado** estou visualizando a tela de Novo apontamento ( fig  - modal add/edit - log time )

&nbsp;&nbsp;&nbsp;&nbsp;**E** não informei **um dos campos obrigatórios abaixo 

    * Tarefa
    * Tipo
    * Data 
    * Inicio
    * Fim

**Quando** clico no botão “Log Time”

**Então** será apresentada uma mensagem de Erro “Favor preencher os campos obrigatórios”

&nbsp;&nbsp;&nbsp;&nbsp;**E** os campos não preenchidos deverão ser identificados com um erro

&nbsp;&nbsp;&nbsp;&nbsp;**E** o apontamento não será salvo 

&nbsp;&nbsp;&nbsp;&nbsp;**E** e o modal deverá permanecer aberto com os valores preenchidos anteriormente 


### CA06 - Erro ao apontar mais de 8h no dia para uma tarefa padrão

**Dado** estou visualizando a tela de Novo apontamento ( fig  - modal add/edit - log time )

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei uma Tarefa atribuída a mim 

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei o Tipo como Padrão

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei a mesma Data dos apontamentos anteriores

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei um intervalo de início e Fim válidos **que o acumulado no dia para o tipo “Padrão” ultrapassa o valor de 8h** 


**Quando** clico no botão “Log Time”

**Então** será apresentada uma mensagem de Erro **“Não é permitido apontar mais de 8h, com o tipo de apontamento Padrão,  Caso tenha HE liberada troque para o tipo “Hora Extra””**

&nbsp;&nbsp;&nbsp;&nbsp;**E** o apontamento não será salvo

&nbsp;&nbsp;&nbsp;&nbsp;**E** o modal deverá permanecer aberto com os valores preenchidos 
anteriormente


### CA07 - Erro ao selecionar Hora Extra ou Banco de Horas sem liberação 

**Dado** estou visualizando a tela de Novo apontamento ( fig  - modal add/edit - log time )

&nbsp;&nbsp;&nbsp;&nbsp;**E** __*Não possuo liberação de apontamento de hora extra ou Banco de Horas*__ 

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei uma Tarefa  atribuída a mim

**Quando** seleciono o Tipo como Hora Extra ou Banco de Horas

**Então** será apresentada uma mensagem de Erro “Não é possível realizar este tipo de apontamento,  entre em contato com o seu gestor. ”

&nbsp;&nbsp;&nbsp;&nbsp;**E** o apontamento não poderá ser salvo 

&nbsp;&nbsp;&nbsp;&nbsp;**E** o modal deverá permanecer aberto com os valores preenchidos anteriormente


### CA08 - Erro ao realizar o apontamento com sobreposição de horários

**Dado** estou visualizando a tela de Novo apontamento ( fig  - modal add/edit - log time )

&nbsp;&nbsp;&nbsp;&nbsp;**E** _já realizei outros apontamentos_

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei uma Tarefa  atribuída a mim

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei o Tipo como Padrão **ou** Hora Extra **ou** Banco de Horas

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei a mesma Data dos apontamentos anteriores

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei um intervalo de início e Fim que sobrepõem um ou mais apontamentos anteriores  

**Quando** clico no botão “Log Time”

**Então** será apresentada uma mensagem de Erro “Não é possível apontar horas sobrepostas, você já possui apontamentos neste intervalo” 

&nbsp;&nbsp;&nbsp;&nbsp;**E** o apontamento não será salvo 

&nbsp;&nbsp;&nbsp;&nbsp;**E** o modal deverá permanecer aberto com os valores preenchidos anteriormente


### CA09 - Erro ao realizar o apontamento de Horas Extra com mais de 80h acumuladas no mês.

**Dado** estou visualizando a tela de Novo apontamento ( fig  - modal add/edit - log time ) 

&nbsp;&nbsp;&nbsp;&nbsp;**E** *já realizei outros apontamentos do tipo Hora Extra no mês* 

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei uma Tarefa  atribuída a mim 

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei o Tipo como Hora Extra 

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei uma data válida no mês 

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei um intervalo de início e Fim validos, *que quando acumulados no mês selecionado somam mais que __80h__*

**Quando** clico no botão “Log Time”

**Então** será apresentada uma mensagem de Erro “Não é possível apontar mais que 80h Extra no mês” 

&nbsp;&nbsp;&nbsp;&nbsp;**E** o apontamento não será salvo 

&nbsp;&nbsp;&nbsp;&nbsp;**E** o modal deverá permanecer aberto com os valores preenchidos anteriormente 


### CA10 - Erro ao realizar o apontamento de Banco de Horas com mais de 40h acumuladas no mês.

**Dado** estou visualizando a tela de Novo apontamento ( fig  - modal add/edit - log time ) 

&nbsp;&nbsp;&nbsp;&nbsp;**E** _já realizei outros apontamentos do tipo Banco de Horas no mês_ 

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei uma Tarefa atribuída a mim 

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei o Tipo como Banco de Horas 

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei uma data válida no mês 

&nbsp;&nbsp;&nbsp;&nbsp;**E** selecionei um intervalo de início e Fim validos, que quando acumulados no mês em questão somam mais que 40h 

**Quando** clico no botão “Log Time”

**Então** será apresentada uma mensagem de Erro “Não é possível apontar mais que 40h no mês para Banco de Horas” 

&nbsp;&nbsp;&nbsp;&nbsp;**E** o apontamento não será salvo 

&nbsp;&nbsp;&nbsp;&nbsp;**E** o modal deverá permanecer aberto com os valores preenchidos anteriormente 


---

## Dados Adicionais

### Propótipos de Tela

#### FIG - Fludo de Navegação {#fig-timesheet-flow}

![alt_text](assets/images/timesheet-flow.png "Fluxo de Navegação")


#### FIG - USER LOG REPORT {#fig-timelog-report}

![alt_text](assets/images/timelog-report.png "Time log Report")

#### FIG  - MODAL ADD/EDIT - LOG TIME {#fig-add-new-log-time}

![alt_text](assets/images/add-new-log-time.png "New Time log Modal")


#### FIG  - MODAL ADD/EDIT - LOG TIME- MISSING REQUIRED FIELDS {#fig-add-new-log-time-required-field}

![alt_text](assets/images/add-new-log-time-required-field.png "New Time log  Required field - Modal")


#### FIG  - MESSAGES {#fig-messages}

![alt_text](assets/images/timelog-messages.png "Messages")


---

## Modelo de dados das bases existentes


### Usuários

```json
[
  {
    "id": 11231,
    "username": "usernameOne",
    "profile": "EMPLOYEE",
    "isOvertimeAllowed": true,
    "isCompensatoryTimeAllowed": false
  },
  {
    "id": 42346,
    "username": "usernameTwo",
    "profile": "EMPLOYEE",
    "isOvertimeAllowed": true,
    "isCompensatoryTimeAllowed": true
  }
]

```

### Projetos
```json
[
  {
    "id": 1211212,
    "key": "TMS-WEB-APP",
    "name": "Timesheet Web APP",
    "startDate": "2021-01-01",
    "endDate": "2021-12-31",
    "status": "Open",
    "tasks": [
      {
        "id": 42341,
        "subject": "Manage Project",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameOther",
          "profile": "EMPLOYEE"
        }
      }
      {
        "id": 42342,
        "subject": "Create APP Front End",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameOne",
          "profile": "EMPLOYEE"
        }
      },
      {
        "id": 42344,
        "subject": "Create APP BackEnd",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameOne",
          "profile": "EMPLOYEE"
        }
      },
      {
        "id": 42345,
        "subject": "Create APP Front End Tests",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameOne",
          "profile": "EMPLOYEE"
        }
      },
      {
        "id": 42346,
        "subject": "Create APP BackEnd Tests",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameOne",
          "profile": "EMPLOYEE"
        }
      }
    ]
  },
  {
    "id": 2345334,
    "key": "TMS-ANDROID-APP",
    "name": "Timesheet Android APP",
    "startDate": "2021-01-01",
    "endDate": "2021-12-31",
    "status": "Open",
    "tasks": [
      {
        "id": 42342,
        "subject": "Create UI",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameOne",
          "profile": "EMPLOYEE"
        }
      },
      {
        "id": 42344,
        "subject": "Create BackEnd",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameOne",
          "profile": "EMPLOYEE"
        }
      },
      {
        "id": 42345,
        "subject": "Create UI Tests",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameOne",
          "profile": "EMPLOYEE"
        }
      },
      {
        "id": 42346,
        "subject": "Create BackEnd Tests",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameOne",
          "profile": "EMPLOYEE"
        }
      }
    ]
  },
  {
    "id": 87347834,
    "key": "TMS-IOS-APP",
    "name": "Timesheet IO APP",
    "startDate": "2021-01-01",
    "endDate": "2021-12-31",
    "status": "Open",
    "tasks": [
      {
        "id": 42342,
        "subject": "Create UI",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameTwo",
          "profile": "EMPLOYEE"
        }
      },
      {
        "id": 42344,
        "subject": "Create BackEnd",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameTwo",
          "profile": "EMPLOYEE"
        }
      },
      {
        "id": 42345,
        "subject": "Create UI Tests",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameTwo",
          "profile": "EMPLOYEE"
        }
      },
      {
        "id": 42346,
        "subject": "Create BackEnd Tests",
        "startDate": "2021-01-01",
        "endDate": "2021-12-31",
        "status": "Open",
        "assignee": {
          "id": 11231,
          "username": "usernameTwo",
          "profile": "EMPLOYEE"
        }
      }
    ]
  }
]

```